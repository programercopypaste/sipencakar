<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <!-- <h4 class="page-title"><?= $position ?></h4> -->
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?= base_url('admin') ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?= base_url('dosen') ?>">Karyawan</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="">Ubah <?= $position ?></a>
                    </li>
                </ul>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-items-center">
                        <h4 class="card-title"><?= $position ?></h4>
                    </div>
                </div>
                <form method="post" action="">
                    <div class="card-body col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control" name="id_soal" hidden="hidden" placeholder="Name" value="<?= $soal['id_soal'] ?>">
                        </div>

                        <div class="form-group">
                            <label>Soal</label>
                            <input type="text" class="form-control" name="soal" placeholder="Name" value="<?= $soal['soal'] ?>">
                            <?= form_error('soal', '<small class="text-danger">', '</small>'); ?>
                        </div>

                        <div class="form-group">
                            <label>bobot</label>
                            <input type="text" class="form-control" name="bobot" placeholder="Name" value="<?= $soal['bobot'] ?>">
                            <?= form_error('bobot', '<small class="text-danger">', '</small>'); ?>
                        </div>

                        <div class="form-group">
                            <label>jawaban</label>
                            <input type="text" class="form-control" name="jawaban" placeholder="Jawaban" value="<?= $soal['jawaban'] ?>">
                            <?= form_error('jawaban', '<small class="text-danger">', '</small>'); ?>
                        </div>

                        <div class="form-group">
                            <label>option1</label>
                            <input type="text" class="form-control" name="option1" placeholder="Name" value="<?= $soal['option1'] ?>">
                            <?= form_error('option1', '<small class="text-danger">', '</small>'); ?>
                        </div>

                        <div class="form-group">
                            <label>option2</label>
                            <input type="text" class="form-control" name="option2" placeholder="Name" value="<?= $soal['option2'] ?>">
                            <?= form_error('option2', '<small class="text-danger">', '</small>'); ?>
                        </div>

                        <div class="form-group">
                            <label>option3</label>
                            <input type="text" class="form-control" name="option3" placeholder="Name" value="<?= $soal['option3'] ?>">
                            <?= form_error('option3', '<small class="text-danger">', '</small>'); ?>
                        </div>

                        <div class="form-group">
                            <label>option4</label>
                            <input type="text" class="form-control" name="option4" placeholder="Name" value="<?= $soal['option4'] ?>">
                            <?= form_error('option4', '<small class="text-danger">', '</small>'); ?>
                        </div>            
                    </div>
                    <div class="card-action">
                        <button type="submit" class="btn btn-primary" name="ubah" onclick="return confirm('Are you sure want to edit?')">Ubah</button>
                        <a href=" <?= base_url('dosen') ?>" class="btn btn-danger">Batal</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>