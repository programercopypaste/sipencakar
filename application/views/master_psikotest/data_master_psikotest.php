<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <!-- <h4 class="page-title">Data <?= $position ?></h4> -->
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?= base_url('admin') ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href=""><?= $position ?></a>
                    </li>
                </ul>
            </div>

            <?php if ($this->session->flashdata('done')) { ?>
                <div class="alert alert-success alert-dismissable" id="close_alert">
                    <h4><?= $this->session->flashdata('done'); ?></h4>
                </div>
            <?php } ?>
            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-items-center">
                        <h4 class="card-title"><?= $position ?></h4>
                        <?php if ($admin['akses'] == "Administrator" || $admin['akses'] == "HRD") { ?>
                            <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
                                <i class="fa fa-plus"></i>
                                Tambah <?= $position ?>
                            </button>
                        <?php } ?>
                    </div>
                </div>
                <div class="card-body">
                    <!-- Modal -->
                    <div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header no-bd">
                                    <h5 class="modal-title">
                                        <span class="fw-mediumbold">
                                            Tambah <?= $position; ?> Baru
                                        </span>
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post" action="">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-default">
                                                    <label>Soal</label>
                                                    <input id="addTime" name="Soal" type="text" class="form-control" placeholder="Soal">
                                                </div>
                                                <?= form_error('Soal', '<small class="text-danger">', '</small>'); ?>
                                                <div class="form-group form-group-default">
                                                    <label>Bobot</label>
                                                    <input id="addTime" name="Bobot" type="text" class="form-control" placeholder="Bobot Soal">
                                                </div>
                                                <?= form_error('Bobot', '<small class="text-danger">', '</small>'); ?>
                                                <div class="form-group form-group-default">
                                                    <label>jawaban</label>
                                                    <input id="addTime" name="Jawaban" type="text" class="form-control" placeholder="Jawaban">
                                                </div>
                                                <?= form_error('Jawaban', '<small class="text-danger">', '</small>'); ?>
                                                <div class="form-group form-group-default">
                                                    <label>Option 1 </label>
                                                    <input id="addTime" name="Option_1" type="text" class="form-control" placeholder="Option_1">

                                                </div>
                                                <div class="form-group form-group-default">
                                                    <label>Option 2</label>
                                                    <input type="text" class="form-control" name="Option_2" placeholder="Option_2" value="">
                                                </div>
                                                <div class="form-group form-group-default">
                                                    <label>Option 3</label>
                                                    <input type="text" class="form-control" name="Option_3" placeholder="Option_3">
                                                </div>
                                                <div class="form-group form-group-default">
                                                    <label>Option 4</label>
                                                    <input type="text" class="form-control" name="Option_4" placeholder="Option_4">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer no-bd">
                                        <button type="submit" id="addRowButton" class="btn btn-primary">Tambah</button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="add-row" class="display table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 6%">No</th>
                                    <th>Soal</th>
                                    <th>Bobot</th>
                                    <th style="width: 10%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($soal as $g) {
                                    ?>
                                    <tr>

                                        <td><?= $no ?></td>
                                        <td><?= $g['soal'] ?></td>
                                        <td><?= $g['bobot'] ?></td>
                                        <td>
                                            <div class="form-button-action">
                                                <a href="<?= base_url('master_psikotest/ubah/') . $g['id_soal'] ?>" class="btn btn-link btn-primary">
                                                    <i class="fas fa-edit">Ubah</i>
                                                </a>
                                                </a>
                                                <a href="<?= base_url('master_psikotest/hapus/') . $g['id_soal'] ?>" class="btn btn-link btn-danger" onclick="return confirm('Are you sure want to delete?')">
                                                    <i class="fas fa-times">Hapus</i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                    $no = $no + 1;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>