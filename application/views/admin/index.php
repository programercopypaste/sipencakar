		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title"><?= $position ?></h4>
					</div>
					<?php if ($this->session->flashdata('done')) { ?>
						<div class="alert alert-success alert-dismissable" id="close_alert">
							<h4><?= $this->session->flashdata('done'); ?></h4>
						</div>
					<?php } ?>

					<?php if ($admin['akses'] == "Administrator" || $admin['akses'] == "HRD") { ?>
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-sm-6 col-md-4">
										<div class="card card-stats card-round">
											<div class="card-body">
												<div class="row align-items-center">
													<div class="col-icon">
														<div class="icon-big text-center icon-primary bubble-shadow-small">
															<i class="far fa-id-card"></i>
														</div>
													</div>
													<div class="col col-stats ml-3 ml-sm-0">
														<div class="numbers">
															<p class="card-category">Jumlah Lowongan Terbuka</p>
															<h4 class="card-title"><?= $loker['jumlah'] ?></h4>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-sm-6 col-md-4">
										<div class="card card-stats card-round">
											<div class="card-body ">
												<div class="row align-items-center">
													<div class="col-icon">
														<div class="icon-big text-center icon-info bubble-shadow-small">
															<i class="far fa-clock"></i>
														</div>
													</div>
													<div class="col col-stats ml-3 ml-sm-0">
														<div class="numbers">
															<p class="card-category">Jumlah Pelamar</p>
															<h4 class="card-title"><?= $jumlah_pelamar['jumlah'] ?></h4>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<?php if ($admin['akses'] == "HRD") { ?>
										<div class="col-sm-6 col-md-4">
											<div class="card card-stats card-round">
												<div class="card-body ">
													<div class="row align-items-center">
														<div class="col-icon">
															<div class="icon-big text-center icon-warning bubble-shadow-small">
																<i class="far fa-clock"></i>
															</div>
														</div>
														<div class="col col-stats ml-3 ml-sm-0">
															<div class="numbers">
																<p class="card-category">Jumlah Pelamar Lolos</p>
																<h4 class="card-title"><?= $jumlah_pelamar_lolos['jumlah'] ?></h4>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
									
									<?php if ($admin['akses'] == "Administrator") { ?>
										<div class="col-sm-6 col-md-4">
											<div class="card card-stats card-round">
												<div class="card-body">
													<div class="row align-items-center">
														<div class="col-icon">
															<div class="icon-big text-center icon-danger bubble-shadow-small">
																<i class="far fa-user-circle"></i>
															</div>
														</div>
														<div class="col col-stats ml-3 ml-sm-0">
															<div class="numbers">
																<p class="card-category">Pengguna</p>
																<h4 class="card-title"><?= $penggunaAplikasi['jumlah'] ?></h4>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>

							</div>

							<div class="col-md-12">
								<div class="row">
									<!-- <div class="col-md-6">
										<div class="card">
											<div class="card-header">
												<div class="card-head-row">
													<div class="card-title">Persentase Alternatif</div>
												</div>
											</div>
											<div class="card-body">
												<div class="card-body">
													<div class="chart-container">
														<canvas id="pieChart" style="width: 50%; height: 50%"></canvas>
													</div>
												</div>
											</div>
										</div>
									</div> -->
									<div class="col-md-12">
										<div class="card">
											<div class="card-header">
												<div class="card-head-row">
													<div class="card-title">Hasil Test Pelamar</div>
												</div>
											</div>
											<div class="card-body">
												<div class="card-body">
													<div class="chart-container" style="width: 100%; height: 10%">
														<canvas id="barChart"></canvas>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- <div class="col-md-6">
								<div class="card card-stats<?php if ($status['0'] == "Perhitungan Selesai") { ?> card-success bg-success-gradient <?php } else {  ?> card-danger bg-danger-gradient <?php } ?>card-round">
									<div class="card-body ">
										<div class="row">
											<div class="col-5">
												<?php
												if ($status['0'] == "Perhitungan Selesai") {
													?>
													<div class="icon-big text-center">
														<i class="flaticon-success"></i>
													</div>
												<?php } else { ?>
													<div class="icon-big text-center">
														<i class="flaticon-error"></i>
													</div>
												<?php } ?>
											</div>
											<div class="col col-stats">
												<div class="numbers">
													<p class="card-category">Status Penilaian</p>
													<h4 class="card-title"><?= $status['0'] ?></h4>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card card-info bg-info-gradient">
									<div class="card-body">
										<h4 class="mb-1 fw-bold">Data Dihitung</h4> -->
										<!-- <div id="task-complete" class="chart-circle mt-4 mb-3"></div> -->
										<!-- <div class="card-desc">
											<h3><?= $status['1'] . " / " . $status['2'] . " Karyawan" ?></h3>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<div class="card-head-row">
											<div class="card-title">Hasil Kinerja Karyawan</div>
											<div class="card-tools">
												<?php if ($admin['akses'] == "Administrator") { ?>
													<?php if ($status['0'] == "Perhitungan Selesai") { ?>
														<a href="<?= base_url('admin/cetak') ?>" class="btn btn-info btn-border btn-round btn-sm">
															<span class="btn-label">
																<i class="fa fa-print"></i>
															</span>
															Print
														</a>
													<?php } ?>
												<?php } ?>
											</div>
										</div>
									</div>
									<div class="card-body">
										<div class="card-body">
											<div class="table-responsive">
												<table id="add-row" class="display table table-striped table-hover">
													<thead>
														<tr>
															<th style="width: 6%">No</th>
															<th>Nama</th>
															<th>NIK</th>
															<th>Hasil</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$no = 1;
														foreach ($hasil as $h) {
															?>
															<tr>
																<td><?= $no ?></td>
																<td><?= $h['nama_karyawan'] ?></td>
																<td><?= $h['nik'] ?></td>
																<td><?= $h['alternatif'] ?></td>
															</tr>
														<?php
															$no = $no + 1;
														}
														?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div> -->
						</div>
					<?php } else if($admin['akses'] == "Pelamar") { ?>
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header">
										<div class="card-head-row">
											<div class="card-title">Selamat Datang</div>
										</div>
									</div>
									<div class="card-body">
										<div class="col-md-12">
											<div class="card">
												<div class="card-header">
													<div class="card-head-row">
														<div class="col-md-6">
															<div class="card-title">Profile Pengguna</div>
														</div>
														<div class="col-md-6">
															<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
																Ubah Data Profile Pangguna
															</button>
														</div>

													</div>
												</div>
												<div class="collapse" id="collapseExample">
													<div class="card-body">
														<label class="col-md-12"> Ubah Data Profile Pengguna </label>
														<form method="post" action="<?= base_url('admin/simpanDataPelamar') ?>">
															<div class="card-body col-md-12">
																<input id="id_pelamar" name="id_pelamar" type="text" hidden="hidden" value="<?= $pengguna[0]['id_pelamar'] ?>">
																<div class="form-group">
																	<label>Nama</label>
																	<input type="text" class="form-control" name="nama" readonly placeholder="Name" value="<?= $pengguna[0]['nama_pelamar'] ?>">
																	<?= form_error('nama', '<small class="text-danger">', '</small>'); ?>
																</div>
				
															<div class="form-group ">
																	<label>Lowongan</label>
																	<select class="form-control" id="exampleFormControlSelect1" name="lowongan">
																		<?php
																		foreach ($lowongan as $l) {
																			if ($pengguna[0]['id_lowongan'] != $l['id_lowongan']) {
																				?>
																				<option value="<?= $l['id_lowongan'] ?>"><?= $l['lowongan'] ?></option>
																			<?php
																				} else {
																					?>
																				<option value="<?= $l['id_lowongan'] ?>" selected><?= $l['lowongan'] ?></option>
																		<?php
																			}
																		}
																		?>
																	</select>
																</div>
																<div class="form-group ">
																	<label>Jenis Kelamin</label>
																	<select class="form-control" id="exampleFormControlSelect1" name="jenis_kelamin">
																		<?php
																		if ($karyawan['jenis_kelamin'] != 'L') {
																			?>
																			<option value="P" selected>Perempuan</option>
																			<option value="L">Laki-laki</option>
																		<?php
																		} else {
																			?>
																			<option value="P">Perempuan</option>
																			<option value="L" selected>Laki-laki</option>
																		<?php
																		}
																		?>
																	</select>
																</div>
																<div class="form-group">
																	<label>Alamat</label>
																	<input type="text" class="form-control" name="alamat" placeholder="Alamat" value="<?= $pengguna[0]['alamat'] ?>">
																</div>
																<div class="form-group">
																	<label>No.HP</label>
																	<input type="text" class="form-control" name="no_hp" placeholder="No.HP" value="<?= $pengguna[0]['no_hp'] ?>">
																</div>
																<div class="form-group">
																	<label>Email</label>
																	<input type="text" class="form-control" name="email" readonly placeholder="Email" value="<?= $pengguna[0]['email'] ?>">
																</div>
															</div>
															<div class="card-action">
																<button type="submit" class="btn btn-primary" name="ubah" onclick="return confirm('Are you sure want to edit?')">Simpan</button>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="card">
												<div class="card-header">
													<div class="card-head-row">
														<div class="col-md-6">
															<div class="card-title">Lowongan</div>
														</div>
														<div class="col-md-6">
															<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseLowongan" aria-expanded="false" aria-controls="collapseExample">
																Lihat Lowongan
															</button>
														</div>

													</div>
												</div>
												<div class="collapse" id="collapseLowongan">
													<div class="card-body">
														<label class="col-md-12"> Lowongan Terbuka </label>
														<?php foreach ($lowongan as $loker) { ?>
															<div class="">
																<div class="card card-stats card-round">
																	<div class="card-body ">
																		<div class="row align-items-center">
																			<div class="col-icon">
																				<div class="icon-big text-center icon-info bubble-shadow-small">
																					<i class="far fa-clock"></i>
																				</div>
																			</div>
																			<div class="col col-stats ml-3 ml-sm-0">
																				<div class="col-md-6">
																					<div class="numbers">
																						<p class="card-category"><?= $loker['lowongan'] ?></p>
																						<h4 class="card-title"></h4>
																					</div>
																				</div>
																				<div class="col-md-6">
																					<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseLowongan_<?= $loker['lowongan'] ?>" aria-expanded="false" aria-controls="collapseExample">
																						Lihat Detail
																					</button>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="collapse" id="collapseLowongan_<?= $loker['lowongan'] ?>">
																<div class="card card-stats card-round">
																	<div class="card-body">
																		<h1 class="col-md-12 mb-3"> Detail Pekerjaan </h1>
																		
																		<hr>
																		
																		<div class="col col-stats ml-3 ml-sm-0">
																			<div class="col-md-6">
																				<h4 class="card-title">Deskripsi Pekerjaan</h4>
																				<br>
																				<textarea style="resize:none; border: none;" id="deskripsi" name="deskripsi" rows="4" cols="50" readonly><?= $loker['deskripsi'] ?></textarea>
																			</div>

																			<div class="col-md-6">
																				<h4 class="card-title">Persyaratan Pekerjaan</h4>
																				<br>
																				<textarea style="resize:none; border: none;" id="deskripsi" name="deskripsi" rows="4" cols="50" readonly><?= $loker['persyaratan'] ?></textarea>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
					

				</div>
			</div>
		</div>
