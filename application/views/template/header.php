<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title><?= $title ?></title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="<?= base_url(); ?>assets/img/icon.ico" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="<?= base_url(); ?>assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Open+Sans:300,400,600,700"]
            },
            custom: {
                "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands"],
                urls: ['<?= base_url(); ?>assets/css/fonts.css']
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/azzara.min.css">

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/demo.css">
</head>

<body>

    <div class="wrapper">
        <!--
			Tip 1: You can change the background color of the main header using: data-background-color="blue | purple | light-blue | green | orange | red"
		-->
        <div class="main-header" style="background-color: #2d394b">
            <!-- Logo Header -->
            <div class="logo-header">

                <!-- <a href="<?= base_url('admin'); ?>" class="logo">
                    <img src="<?= base_url(); ?>assets/img/logoazzara.svg" alt="navbar brand" class="navbar-brand">
                </a> -->
                <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                        <i class="fa fa-bars"></i>
                    </span>
                </button>
                <button class="topbar-toggler more"><i class="fa fa-ellipsis-v"></i></button>
                <div class="navbar-minimize">
                    <button class="btn btn-minimize btn-rounded">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>
            </div>
            <!-- End Logo Header -->

            <!-- Navbar Header -->
            <nav class="navbar navbar-header navbar-expand-lg">

                <div class="container-fluid">
                    <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                        <li class="nav-item dropdown hidden-caret">
                            <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                                <div class="avatar-sm">
                                    <img src="<?= base_url('assets/img/profil/') . $admin['foto']; ?>" alt="..." class="avatar-img rounded-circle">
                                </div>
                            </a>
                            <ul class="dropdown-menu dropdown-user animated fadeIn">
                                <li>
                                    <div class="user-box">
                                        <div class="u-text">
                                            <h4 class="text-bold"><?= $admin['nama']; ?></h4>
                                            <p class="text-muted text-bold"><?= $admin['email']; ?></p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="<?= base_url('admin/profil'); ?>">Profil Saya</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- End Navbar -->
        </div>

        <!-- Sidebar -->
        <div class="sidebar" style="background: #38475d">

            <div class="sidebar-background"></div>
            <div class="sidebar-wrapper scrollbar-inner">
                <div class="sidebar-content">
                    <div class="user">
                        <div class="avatar-sm float-left mr-2">
                            <img src="<?= base_url('assets/img/profil/') . $admin['foto']; ?>" alt="..." class="avatar-img rounded-circle">
                        </div>
                        <div class="info">
                            <a href="<?= base_url('admin/profil'); ?>">
                                <span>
                                    <p class = "text-white mb-0 fw-bold"> <?= $admin['nama']; ?> <p> 
                                    <span class="user-level text-white fw-bold"><?= $admin['akses']; ?></span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <ul class="nav">
                        <li class="nav-item <?php if ($position == 'Halaman Utama') { ?> active <?php } ?>">
                            <a href="<?= base_url('admin') ?>">
                                <i class="fas fa-home text-white"></i>
                                <p class="text-white">Dashboard</p>
                            </a>
                        </li>

                        <?php if ($admin['akses'] == "Administrator" || $admin['akses'] == "HRD") { ?>
                            <li class="nav-item" >
                                <a data-toggle="collapse" href="#datamaster" class="collapsed" aria-expanded="false">
                                    <i class="fas fa-layer-group text-white"></i>
                                    <p class = "text-white">Data Master</p>
                                    <span class="caret"></span>
                                </a>
                                <div class="collapse" id="datamaster">
                                    <ul class="nav nav-collapse">
                                        <li class="nav-item <?php if ($position == 'Lowongan') { ?> active <?php } ?>">
                                            <a href="<?= base_url('Lowongan') ?>">
                                                <i class="fas fa-book text-white"></i>
                                                <p class = "text-white">Lowongan</p>
                                            </a>
                                        </li>
                                        <?php if ($admin['akses'] == "Administrator") { ?>
                                            <li class="nav-item <?php if ($position == 'HRD') { ?> active <?php } ?>">
                                                <a href="<?= base_url('hrd') ?>">
                                                    <i class="fas fa-book text-white"></i>
                                                    <p class = "text-white">HRD</p>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        <li class="nav-item <?php if ($position == 'Pelamar') { ?> active <?php } ?>">
                                            <a href="<?= base_url('pelamar') ?>">
                                                <i class="fas fa-user-tie text-white"></i>
                                                <p class = "text-white">Pelamar</p>
                                            </a>
                                        </li>
                                        <li class="nav-item <?php if ($position == 'Soal Psikotest') { ?> active <?php } ?>">
                                            <a href="<?= base_url('master_psikotest') ?>">
                                                <i class="fas fa-user-tie text-white"></i>
                                                <p class = "text-white">Soal Psikotest</p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="nav-item <?php if ($position == 'Kriteria') { ?> active <?php } ?>">
                                <a href="<?= base_url('kriteria') ?>">
                                    <i class="fas fa-list-ul text-white"></i>
                                    <p class = "text-white">Kriteria</p>
                                </a>
                            </li>
                            <li class="nav-item <?php if ($position == 'Alternatif') { ?> active <?php } ?>">
                                <a href="<?= base_url('alternatif') ?>">
                                    <i class="fas fa-certificate text-white"></i>
                                    <p class = "text-white">Alternatif</p>
                                </a>
                            </li>

                            <li class="nav-item <?php if ($position == 'Nilai') { ?> active <?php } ?>">
                                <a href="<?= base_url('nilai') ?>">
                                    <i class="fas fa-clipboard text-white"></i>
                                    <p class = "text-white">Nilai</p>
                                </a>
                            </li>
                        <?php } ?>

                        <?php if ($admin['akses'] == "Administrator") { ?>
                            <li class="nav-item <?php if ($position == 'Pengguna') { ?> active <?php } ?>">
                                <a href="<?= base_url('admin/pengguna') ?>">
                                    <i class="fas fa-user-cog text-white"></i>
                                    <p class = "text-white">Pengguna</p>
                                </a>
                            </li>
                        <?php } ?>

                        <?php if ($admin['akses'] == "Pelamar") { ?>
                            <?php if ($hasil_test == 0) { ?>
                                <li class="nav-item <?php if ($position == 'psikotest') { ?> active <?php } ?>">
                                    <a href="<?= base_url('psikotest') ?>">
                                        <i class="fas fa-clipboard text-white"></i>
                                        <p class = "text-white">Psikotest</p>
                                    </a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                        
                        <?php if ($admin['akses'] == "Administrator" || $admin['akses'] == "HRD") { ?>
                            <li class="nav-item">
                            <a data-toggle="collapse" href="#base" class="collapsed" aria-expanded="false">
                                <i class="fas fa-layer-group text-white"></i>
                                <p class = "text-white">Laporan</p>
                                <span class="caret"></span>
                            </a>
                            <div class="collapse" id="base" style="">
                                <ul class="nav nav-collapse">
                                    <li>
                                        <a href="<?= base_url('laporan/laporan_penilai/karyawan') ?>">
                                            <i class="fas fa-clipboard text-white"></i>
                                            <span class = "text-white">Laporan Rekap Test Psikotest</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            </li>
                        <?php } ?>

                    </ul>
                </div>
            </div>
        </div>
        <!-- End Sidebar -->

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Are you sure want to logout?</div>
                    <div class="modal-footer">
                        <a class="btn btn-primary" href="<?= base_url('auth/logout'); ?>">Logout</a>
                        <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>