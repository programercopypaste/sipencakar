<?php

class pelamar_Model extends CI_Model
{
    //Akun Aktif
    public function admin_Active()
    {
        return $this->db->get_where('admin', ['email' => $this->session->userdata('email')])->row_array();
    }

    public function get_AllPelamar()
    {
        $query = "SELECT * FROM `pelamar` g 
        join `lowongan` m on g.id_lowongan = m.id_lowongan 
        ORDER BY g.id_pelamar";
        return $this->db->query($query)->result_array();
    }

    public function get_Pelamar($admin)
    {
        $query = "SELECT * FROM `pelamar` g 
        join `lowongan` m on g.id_lowongan = m.id_lowongan WHERE g.id_admin = ? ORDER BY g.id_pelamar";
        return $this->db->query($query, $admin)->result_array();
    }

    public function get_ById($id)
    {
        $query = "SELECT * FROM `pelamar` g 
        join `lowongan` m on g.id_lowongan = m.id_lowongan WHERE g.id_pelamar = ?";
        return $this->db->query($query, $id)->row_array();
    }

    public function get_Penilai()
    {
        return $this->db->get_where('admin', ['akses' => "Penilai"])->result_array();
    }

    public function get_lowongan()
    {
        return $this->db->get('lowongan')->result_array();
    }

    public function add()
    {
        $data = [
            'id_lowongan' => $this->input->post('lowongan', true),
            'nama_pelamar' => htmlspecialchars($this->input->post('nama', true)),
            'nik' => htmlspecialchars($this->input->post('nik', true)),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'alamat' => $this->input->post('alamat'),
            'no_hp' => $this->input->post('no_hp'),
            'email' => $this->input->post('email'),
        ];

        $this->db->insert('pelamar', $data);
    }

    public function insert_Pengguna()
    {
        $data = [
            'email' => htmlspecialchars($this->input->post('email', true)),
            'password' => md5('pelamar123'),
            'nama' => htmlspecialchars($this->input->post('nama', true)),
            'foto' => 'default.png',
            'akses' => 'Pelamar'
        ];

        $this->db->insert('admin', $data);
    }

    public function edit($id)
    {
        $data = [
            'id_lowongan' => $this->input->post('lowongan', true),
            'nama_pelamar' => htmlspecialchars($this->input->post('nama', true)),
            'nik' => htmlspecialchars($this->input->post('nik', true)),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'alamat' => $this->input->post('alamat'),
            'no_hp' => $this->input->post('no_hp'),
            'email' => $this->input->post('email'),
        ];

        $this->db->where('id_pelamar', $id);
        $this->db->update('pelamar', $data);
    }

    public function delete($id)
    {
        $this->db->delete('pelamar', ['id_pelamar' => $id]);
    }
}
