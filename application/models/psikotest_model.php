<?php

class psikotest_model extends CI_Model
{
    public function admin_Active()
    {
        return $this->db->get_where('admin', ['email' => $this->session->userdata('email')])->row_array();
    }
    
    // Master Soal Psikotest
    public function add()
    {
        $data = [
            'soal' => $this->input->post('Soal', true),
            'bobot' => $this->input->post('Bobot', true),
            'jawaban' => htmlspecialchars($this->input->post('Jawaban', true)),
            'option1' => $this->input->post('Option_1'),
            'option2' => $this->input->post('Option_2'),
            'option3' => $this->input->post('Option_3'),
            'option4' => $this->input->post('Option_4'),
        ];

        $this->db->insert('soal_psikotest', $data);
    }

    public function edit($id)
    {
        $data = [
            'soal' => $this->input->post('soal', true),
            'bobot' => $this->input->post('bobot', true),
            'jawaban' => htmlspecialchars($this->input->post('jawaban', true)),
            'option1' => $this->input->post('option1'),
            'option2' => $this->input->post('option2'),
            'option3' => $this->input->post('option3'),
            'option4' => $this->input->post('option4'),
        ];

        $this->db->where('id_soal', $id);
        $this->db->update('soal_psikotest', $data);
    }

    public function delete($id)
    {
        $this->db->delete('soal_psikotest', ['id_soal' => $id]);
    }

    public function get_ById($id)
    {
        $query = "SELECT * FROM `soal_psikotest` m 
        WHERE m.id_soal = ?";
        return $this->db->query($query, $id)->row_array();
    }
    // Test Psikotest
    public function getSoalTest(){
        $query = "select * from soal_psikotest";
        return $this->db->query($query)->result_array();
    }

    public function getPengguna($where){
        $query = "select * from pelamar where $where";
        return $this->db->query($query)->result_array();
    }

    public function getHasiTest($where){
        $query = "select COUNT(id_pelamar) as jumlah from hasil_psikotest  where $where";
        return $this->db->query($query)->result_array();
    }

    public function insertData($data){
        $this->db->insert("hasil_psikotest", $data);
        return true;
    }

}
?>