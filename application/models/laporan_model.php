<?php

class laporan_model extends CI_Model
{

	public function admin_Active()
    {
        return $this->db->get_where('admin', ['email' => $this->session->userdata('email')])->row_array();
    }
    

    //laproranrekap
    function getdinilai($id_periode, $categori){
        if($categori=='1'){
           $query = "SELECT a.id_karyawan, a.nik, a.nama_karyawan
                FROM karyawan a
                WHERE a.id_karyawan in (SELECT id_karyawan FROM nilai where id_periode = $id_periode GROUP BY id_karyawan)
                GROUP BY a.id_karyawan";
        }else{
            $query = "SELECT a.id_karyawan, a.nik, a.nama_karyawan
                FROM karyawan a
                WHERE a.id_karyawan NOT in (SELECT id_karyawan FROM nilai where id_periode = $id_periode GROUP BY id_karyawan)
                GROUP BY a.id_karyawan";
        }

        return $this->db->query($query)->result_array();
    }

    function getpenilai($categori){
        if($categori=='1'){
           $query ="SELECT *
                from pelamar a
                INNER JOIN hasil_psikotest b ON a.id_pelamar = b.id_pelamar
                INNER JOIN lowongan c ON b.id_lowongan = c.id_lowongan
                WHERE b.`status` = 'LULUS TEST'
               ";
        }else{
            $query = "SELECT *
            from pelamar a
            INNER JOIN hasil_psikotest b ON a.id_pelamar = b.id_pelamar
            INNER JOIN lowongan c ON b.id_lowongan = c.id_lowongan
            WHERE b.`status` = 'TIDAK LULUS TEST'
           ";
        }

        return $this->db->query($query)->result_array();
    }


}