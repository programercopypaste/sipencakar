<?php
defined('BASEPATH') or exit('No direct script access allowed');

class kriteria extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('email')) {
            redirect('auth');
        }
        if ($this->session->userdata('akses') == 'Pelamar') {
            redirect('auth/blok');
        }
        $this->load->model('kriteria_model');
        $this->load->model('psikotest_model');

    }

    public function index()
    {
        $data['admin'] = $this->kriteria_model->admin_Active();
        $data['title'] = 'SIPENCAKAR - Kriteria';
        $data['position'] = 'Kriteria';
        $data['kriteria'] = $this->kriteria_model->get_AllKriteria();
        $nama = $data['admin']['nama'];
        $email = $data['admin']['email'];        
        $where = "nama_pelamar = '$nama' AND email='$email'";
        $data['pengguna'] = $this->psikotest_model->getPengguna($where);
        
        if($data['admin']['akses'] == 'Pelamar') {
            $id_pelamar= $data['pengguna'][0]['id_pelamar'];
            $where = "id_pelamar = '$id_pelamar'";
            $hasil_test = $this->psikotest_model->getHasiTest($where);
            $data['hasil_test'] = $hasil_test[0]['jumlah'];
        } else {
            $data['hasil_test'] = 0;
        }

        $this->load->view('template/header', $data);
        $this->load->view('kriteria/index', $data);
        $this->load->view('template/footer');
    }

    public function ubah($id)
    {
        $data['admin'] = $this->kriteria_model->admin_Active();
        $data['title'] = 'SIPENCAKAR - Kriteria';
        $data['position'] = 'Ubah Kriteria';
        $data['kriteria'] = $this->kriteria_model->get_ById($id);
        $data['jenis'] = ['Benefit', 'Cost'];
        $data['bobot'] = ['1', '2', '3', '4', '5'];

        $nama = $data['admin']['nama'];
        $email = $data['admin']['email'];        
        $where = "nama_pelamar = '$nama' AND email='$email'";
        $data['pengguna'] = $this->psikotest_model->getPengguna($where);
        
        if($data['admin']['akses'] == 'Pelamar') {
            $id_pelamar= $data['pengguna'][0]['id_pelamar'];
            $where = "id_pelamar = '$id_pelamar'";
            $hasil_test = $this->psikotest_model->getHasiTest($where);
            $data['hasil_test'] = $hasil_test[0]['jumlah'];
        } else {
            $data['hasil_test'] = 0;
        }

        $this->form_validation->set_rules('kriteria', 'Kriteria', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('template/header', $data);
            $this->load->view('kriteria/ubah', $data);
            $this->load->view('template/footer');
        } else {
            $this->kriteria_model->edit($id);
            $this->session->set_flashdata('done', 'Data berhasil diubah');
            redirect('kriteria');
        }
    }
}
