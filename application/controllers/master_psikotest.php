<?php
defined('BASEPATH') or exit('No direct script access allowed');

class master_psikotest extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('email')) {
            redirect('auth');
        }

        if ($this->session->userdata('akses') == 'Pelamar') {
            redirect('auth/blok');
        }

        $this->load->model('psikotest_model');
    }

    public function index()
    {
        $data['admin'] = $this->psikotest_model->admin_Active();
        $admin = $data['admin']['id_admin'];
        $data['title'] = 'SIPENCAKAR - Master Soal Psikotest';
        $data['position'] = 'Soal Psikotest';
        $data['soal'] = $this->psikotest_model->getSoalTest();

        $nama = $data['admin']['nama'];
        $email = $data['admin']['email'];        
        $where = "nama_pelamar = '$nama' AND email='$email'";
        $data['pengguna'] = $this->psikotest_model->getPengguna($where);
        
        if($data['admin']['akses'] == 'Pelamar') {
            $id_pelamar= $data['pengguna'][0]['id_pelamar'];
            $where = "id_pelamar = '$id_pelamar'";
            $hasil_test = $this->psikotest_model->getHasiTest($where);
            $data['hasil_test'] = $hasil_test[0]['jumlah'];
        } else {
            $data['hasil_test'] = 0;
        }

        $this->form_validation->set_rules('Soal', 'Soal', 'required|trim');
        $this->form_validation->set_rules('Bobot', 'Bobot', 'required|trim');
        $this->form_validation->set_rules('Jawaban', 'Jawaban', 'required|trim');
        $this->form_validation->set_rules('Option_1', 'Option_1', 'required|trim');
        $this->form_validation->set_rules('Option_2', 'Option_2', 'required|trim');
        $this->form_validation->set_rules('Option_3', 'Option_3', 'required|trim');
        $this->form_validation->set_rules('Option_4', 'Option_4', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('template/header', $data);
            $this->load->view('master_psikotest/data_master_psikotest', $data);
            $this->load->view('template/footer');
        } else {
            $this->psikotest_model->add();
            $this->session->set_flashdata('done', 'Data berhasil ditambah');
            redirect('master_psikotest');
        }
    }

    public function ubah($id)
    {
        $data['admin'] = $this->psikotest_model->admin_Active();
        $data['title'] = 'SIPENCAKAR - master_psikotest';
        $data['position'] = 'master_psikotest';
        $data['soal'] = $this->psikotest_model->get_ById($id);

        $nama = $data['admin']['nama'];
        $email = $data['admin']['email'];        
        $where = "nama_pelamar = '$nama' AND email='$email'";
        $data['pengguna'] = $this->psikotest_model->getPengguna($where);
        
        if($data['admin']['akses'] == 'Pelamar') {
            $id_pelamar= $data['pengguna'][0]['id_pelamar'];
            $where = "id_pelamar = '$id_pelamar'";
            $hasil_test = $this->psikotest_model->getHasiTest($where);
            $data['hasil_test'] = $hasil_test[0]['jumlah'];
        } else {
            $data['hasil_test'] = 0;
        }

        $this->form_validation->set_rules('soal', 'soal', 'required|trim');
        $this->form_validation->set_rules('bobot', 'bobot', 'required|trim');
        $this->form_validation->set_rules('jawaban', 'jawaban', 'required|trim');
        $this->form_validation->set_rules('option1', 'option1', 'required|trim');
        $this->form_validation->set_rules('option2', 'option2', 'required|trim');
        $this->form_validation->set_rules('option3', 'option3', 'required|trim');
        $this->form_validation->set_rules('option4', 'option4', 'required|trim');
        
        if ($this->form_validation->run() == false) {
            $this->load->view('template/header', $data);
            $this->load->view('master_psikotest/ubah', $data);
            $this->load->view('template/footer');
        } else {
            $this->psikotest_model->edit($id);
            $this->session->set_flashdata('done', 'Data berhasil diubah');
            redirect('master_psikotest');
        }
    }
    
    public function hapus($id)
    {
        $this->psikotest_model->delete($id);
        $this->session->set_flashdata('done', 'Data berhasil dihapus');
        redirect('master_psikotest');
    }

}
?>