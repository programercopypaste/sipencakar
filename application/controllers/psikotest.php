<?php
defined('BASEPATH') or exit('No direct script access allowed');

class psikotest extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('email')) {
            redirect('auth');
        }
       
        $this->load->model('psikotest_model');
        $this->load->model('pelamar_model');
        $this->load->model('nilai_model');
    }

    public function index()
    {
        $data['admin'] = $this->nilai_model->admin_Active();
        $admin = $data['admin']['id_admin'];
        $nama = $data['admin']['nama'];
        $email = $data['admin']['email'];
        $data['title'] = 'SIPENCAKAR - Psikotest';
        $data['position'] = 'Psikotest';

        $where = "nama_pelamar = '$nama' AND email='$email'";
        
        $data['soal'] = $this->psikotest_model->getSoalTest();
        $data['pengguna'] = $this->psikotest_model->getPengguna($where);
        
        
        if($data['admin']['akses'] == 'Pelamar') {
            $id_pelamar= $data['pengguna'][0]['id_pelamar'];
            $where = "id_pelamar = '$id_pelamar'";
            $hasil_test = $this->psikotest_model->getHasiTest($where);
            $data['hasil_test'] = $hasil_test[0]['jumlah'];
        } else {
            $data['hasil_test'] = 0;
        }

        $this->load->view('template/header', $data);
        $this->load->view('psikotest/psikotest', $data);
        $this->load->view('template/footer');
    }

    public function simpan(){
        $soal= $this->psikotest_model->getSoalTest();
        
        $akumulasi = 0;
        $nilai_akumulasi = 0;
        for ($i = 0; $i < count($soal); $i++) {
            $jawaban_soal = $this->input->post($i + 1,TRUE);       
        
            if ($jawaban_soal == $soal[$i]['jawaban']) {
                $akumulasi = floatval($akumulasi) + 1;
                $nilai_akumulasi = floatval($nilai_akumulasi) + floatval($soal[$i]['bobot']);
            } else {
                $akumulasi = floatval($akumulasi);
                $nilai_akumulasi = floatval($nilai_akumulasi);
            }
        }

        if ($akumulasi == count($soal)) {
            $grade = 'Amat Baik';
            $status = 'LULUS TEST';
        } else if ($akumulasi >= floatval(count($soal) - 2)) {
            $grade = 'Baik';
            $status = 'LULUS TEST';
        } else if ($akumulasi >= floatval(count($soal) - 4)) {
            $grade = 'Cukup';
            $status = 'TIDAK LULUS TEST';
        } else if ($akumulasi >= floatval(count($soal) - 6)) {
            $grade = 'Kurang';
            $status = 'TIDAK LULUS TEST';
        }

        $data = array(
            'id_pelamar'=> $this->input->post('id_pelamar',TRUE),
            'id_lowongan'=> $this->input->post('id_lowongan',TRUE),
            'nilai_akumulasi' => $nilai_akumulasi,
            'alternatif_grade'=> $grade,
            'status' => $status,
        );

        $insert = $this->psikotest_model->insertData($data);

        if ($insert) {
			$this->session->set_flashdata('done', 'Success, Save Psikotest');
			redirect('admin');
		} else {
			$this->session->set_flashdata('error', 'Success, Save Psikotest');
			redirect('psikotest');
		}
    
    }
}