<?php
defined('BASEPATH') or exit('No direct script access allowed');

class lowongan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('email')) {
            redirect('auth');
        }
        if ($this->session->userdata('akses') == 'Pelamar') {
            redirect('auth/blok');
        }
        $this->load->model('lowongan_model');
        $this->load->model('psikotest_model');
    }

    public function index()
    {
        $data['admin'] = $this->lowongan_model->admin_Active();
        $data['title'] = 'SISPENCAKAR - Lowongan';
        $data['position'] = 'Lowongan';
        $data['lowongan'] = $this->lowongan_model->get_AllLowongan();
        $data['status'] = array('0', '1');
        $nama = $data['admin']['nama'];
        $email = $data['admin']['email'];        
        $where = "nama_pelamar = '$nama' AND email='$email'";
        $data['pengguna'] = $this->psikotest_model->getPengguna($where);
        
        if($data['admin']['akses'] == 'Pelamar') {
            $id_pelamar= $data['pengguna'][0]['id_pelamar'];
            $where = "id_pelamar = '$id_pelamar'";
            $hasil_test = $this->psikotest_model->getHasiTest($where);
            $data['hasil_test'] = $hasil_test[0]['jumlah'];
        } else {
            $data['hasil_test'] = 0;
        }

        $this->form_validation->set_rules('lowongan', 'Lowongan', 'required|trim');
        $this->form_validation->set_rules('status', 'Status', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('template/header', $data);
            $this->load->view('lowongan/index', $data);
            $this->load->view('template/footer');
        } else {
            $this->lowongan_model->add();
            $this->session->set_flashdata('done', 'Data berhasil ditambah');
            redirect('lowongan');
        }
    }

    public function ubah($id)
    {
        $data['admin'] = $this->lowongan_model->admin_Active();
        $data['title'] = 'SISPENCAKAR - Lowongan';
        $data['position'] = 'Lowongan';
        $data['lowongan'] = $this->lowongan_model->get_ById($id);
        $data['status'] = array('0', '1');

        $nama = $data['admin']['nama'];
        $email = $data['admin']['email'];        
        $where = "nama_pelamar = '$nama' AND email='$email'";
        $data['pengguna'] = $this->psikotest_model->getPengguna($where);
        
        if($data['admin']['akses'] == 'Pelamar') {
            $id_pelamar= $data['pengguna'][0]['id_pelamar'];
            $where = "id_pelamar = '$id_pelamar'";
            $hasil_test = $this->psikotest_model->getHasiTest($where);
            $data['hasil_test'] = $hasil_test[0]['jumlah'];
        } else {
            $data['hasil_test'] = 0;
        }
        
        if($data['admin']['akses'] == 'Pelamar') {
            $id_pelamar= $data['pengguna'][0]['id_pelamar'];
            $where = "id_pelamar = '$id_pelamar'";
            $hasil_test = $this->psikotest_model->getHasiTest($where);
            $data['hasil_test'] = $hasil_test[0]['jumlah'];
        } else {
            $data['hasil_test'] = 0;
        }

        $this->form_validation->set_rules('lowongan', 'Lowongan', 'required|trim');
        $this->form_validation->set_rules('status', 'Status', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('template/header', $data);
            $this->load->view('lowongan/ubah', $data);
            $this->load->view('template/footer');
        } else {
            $this->lowongan_model->edit($id);
            $this->session->set_flashdata('done', 'Data berhasil diubah');
            redirect('lowongan');
        }
    }

     public function hapus($id)
    {
        $this->lowongan_model->delete($id);
        $this->session->set_flashdata('done', 'Data berhasil dihapus');
        redirect('lowongan');
    }
}