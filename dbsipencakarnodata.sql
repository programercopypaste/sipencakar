-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.22-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for dbsipencakar
CREATE DATABASE IF NOT EXISTS `dbsipencakar` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `dbsipencakar`;

-- Dumping structure for table dbsipencakar.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `akses` varchar(15) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf32;

-- Dumping data for table dbsipencakar.admin: ~8 rows (approximately)
REPLACE INTO `admin` (`id_admin`, `email`, `password`, `nama`, `foto`, `akses`) VALUES
	(1, 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'admin.png', 'Administrator');

-- Dumping structure for table dbsipencakar.alternatif
CREATE TABLE IF NOT EXISTS `alternatif` (
  `id_alternatif` int(11) NOT NULL AUTO_INCREMENT,
  `nama_alternatif` varchar(30) NOT NULL,
  `nilai1` int(11) NOT NULL,
  `nilai2` int(11) NOT NULL,
  `nilai3` int(11) NOT NULL,
  `nilai4` int(11) NOT NULL,
  PRIMARY KEY (`id_alternatif`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf32;

-- Dumping data for table dbsipencakar.alternatif: ~4 rows (approximately)
REPLACE INTO `alternatif` (`id_alternatif`, `nama_alternatif`, `nilai1`, `nilai2`, `nilai3`, `nilai4`) VALUES
	(1, 'Amat baik', 1, 1, 2, 4),
	(2, 'Baik', 3, 2, 3, 3),
	(3, 'Cukup', 1, 3, 3, 2),
	(4, 'Kurang', 4, 3, 1, 1);

-- Dumping structure for table dbsipencakar.hasil_psikotest
CREATE TABLE IF NOT EXISTS `hasil_psikotest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelamar` int(11) NOT NULL,
  `id_lowongan` int(11) NOT NULL DEFAULT 0,
  `nilai_akumulasi` double NOT NULL DEFAULT 0,
  `alternatif_grade` char(50) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table dbsipencakar.hasil_psikotest: ~4 rows (approximately)

-- Dumping structure for table dbsipencakar.hrd
CREATE TABLE IF NOT EXISTS `hrd` (
  `id_hrd` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(50) CHARACTER SET utf32 NOT NULL DEFAULT '',
  `nama` varchar(50) CHARACTER SET utf32 DEFAULT NULL,
  `jenis_kelamin` varchar(50) CHARACTER SET utf32 DEFAULT NULL,
  `alamat` varchar(200) CHARACTER SET utf32 DEFAULT NULL,
  `no_hp` varchar(14) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_hrd`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table dbsipencakar.hrd: ~5 rows (approximately)

-- Dumping structure for table dbsipencakar.kriteria
CREATE TABLE IF NOT EXISTS `kriteria` (
  `id_kriteria` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kriteria` varchar(50) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `bobot` int(11) NOT NULL,
  `keterangan` varchar(120) NOT NULL,
  PRIMARY KEY (`id_kriteria`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf32;

-- Dumping data for table dbsipencakar.kriteria: ~6 rows (approximately)
REPLACE INTO `kriteria` (`id_kriteria`, `nama_kriteria`, `jenis`, `bobot`, `keterangan`) VALUES
	(1, 'C1', 'Benefit', 3, 'Apakah jenis test sudah sesuai untuk pelamar'),
	(2, 'C2', 'Benefit', 3, 'Saya mengetahui bahwa dalam pelaksanaan proses rekrutmen perusahaan mengumumkan melalui media massa'),
	(3, 'C3', 'Benefit', 3, 'Perekrutan karyawan dilaksanakan untuk mengisi jabatan yang kosong.'),
	(4, 'C4', 'Benefit', 3, 'Pelamar mengikuti test online yang telah disiapkan ?'),
	(5, 'C5', 'Benefit', 3, 'Pelamar melakukan tes wawancara'),
	(6, 'C6', 'Benefit', 3, 'Pelamar mempunyai catatan kesehatan yang baik.');

-- Dumping structure for table dbsipencakar.lowongan
CREATE TABLE IF NOT EXISTS `lowongan` (
  `id_lowongan` int(11) NOT NULL AUTO_INCREMENT,
  `lowongan` varchar(50) NOT NULL DEFAULT '',
  `deskripsi` longtext NOT NULL,
  `persyaratan` longtext NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id_lowongan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table dbsipencakar.lowongan: ~6 rows (approximately)
REPLACE INTO `lowongan` (`id_lowongan`, `lowongan`, `deskripsi`, `persyaratan`, `status`) VALUES
	(1, 'General  Manager', '1.Mengerjakan \r\n2.pembuatan model pakaian segala macam yang dibutuhkan', '1.Mengerjakan \r\n2.pembuatan model pakaian segala macam yang dibutuhkan', 0),
	(2, 'IT', '', '', 0),
	(4, 'IT Support', '', '', 0),
	(6, 'Kasir', '', '', 1);

-- Dumping structure for table dbsipencakar.nilai
CREATE TABLE IF NOT EXISTS `nilai` (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_admin` int(11) NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `id_pelamar` int(11) NOT NULL,
  `id_lowongan` int(11) NOT NULL,
  `nilai_pelamar` int(11) NOT NULL,
  PRIMARY KEY (`id_nilai`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf32;

-- Dumping data for table dbsipencakar.nilai: ~62 rows (approximately)

-- Dumping structure for table dbsipencakar.pelamar
CREATE TABLE IF NOT EXISTS `pelamar` (
  `id_pelamar` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pelamar` varchar(100) NOT NULL,
  `nik` varchar(30) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `no_hp` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `id_lowongan` int(11) NOT NULL,
  PRIMARY KEY (`id_pelamar`,`id_lowongan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf32;

-- Dumping data for table dbsipencakar.pelamar: ~9 rows (approximately)

-- Dumping structure for table dbsipencakar.penilaian
CREATE TABLE IF NOT EXISTS `penilaian` (
  `id_penilaian` int(11) NOT NULL AUTO_INCREMENT,
  `id_nilai` int(11) NOT NULL,
  `id_alternatif` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `normalisasi` double NOT NULL,
  `terbobot` double NOT NULL,
  `nilai_akhir` double NOT NULL,
  `rank` int(11) NOT NULL,
  PRIMARY KEY (`id_penilaian`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf32;

-- Dumping data for table dbsipencakar.penilaian: ~0 rows (approximately)

-- Dumping structure for table dbsipencakar.soal_psikotest
CREATE TABLE IF NOT EXISTS `soal_psikotest` (
  `id_soal` int(1) NOT NULL AUTO_INCREMENT,
  `soal` varchar(200) NOT NULL DEFAULT '0',
  `bobot` varchar(200) NOT NULL DEFAULT '0',
  `jawaban` varchar(200) NOT NULL DEFAULT '0',
  `id_option1` char(1) NOT NULL DEFAULT 'A',
  `option1` varchar(50) NOT NULL,
  `id_option2` char(1) NOT NULL DEFAULT 'B',
  `option2` varchar(200) NOT NULL,
  `id_option3` char(1) NOT NULL DEFAULT 'C',
  `option3` varchar(200) NOT NULL,
  `id_option4` char(1) NOT NULL DEFAULT 'D',
  `option4` varchar(200) NOT NULL,
  PRIMARY KEY (`id_soal`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table dbsipencakar.soal_psikotest: ~0 rows (approximately)
REPLACE INTO `soal_psikotest` (`id_soal`, `soal`, `bobot`, `jawaban`, `id_option1`, `option1`, `id_option2`, `option2`, `id_option3`, `option3`, `id_option4`, `option4`) VALUES
	(1, 'Jika 1 + 4 =5, 2 + 5 =12, dan 3 + 6 =21 maka 8 + 11 =?', '3', '', 'A', '96', 'B', '90', 'C', '67', 'D', '12'),
	(2, 'jari dengan tangan ibarat daun dengan ?\r\n', '3', 'A', 'A', 'ranting', 'B', '-', 'C', '-', 'D', '-'),
	(3, 'setengah dari seperempat dari 4000 adalah?\r\n', '3', 'A', 'A', '500', 'B', '-', 'C', '-', 'D', '-'),
	(4, 'berapa 20% dari 30.000\r\n', '3', 'A', 'A', '6000', 'B', '-', 'C', '-', 'D', '-'),
	(5, '4 - 7 - 12 - 15 - 20 =?\r\n', '3', 'A', 'A', '23', 'B', '-', 'C', '-', 'D', '-'),
	(6, '(persamaan kata) insomnia =?\r\n', '3', 'A', 'A', 'Tidak bisa tidur', 'B', '-', 'C', '-', 'D', '-'),
	(7, 'bongsor = ?\r\n', '3', 'A', 'A', 'kerdil', 'B', '-', 'C', '-', 'D', '-'),
	(8, 'diketahui balok dengan ukuran (12 x 10 x 8) berapa luas balok seluruhnya\r\n', '3', 'A', 'A', '592', 'B', '-', 'C', '-', 'D', '-'),
	(9, 'anda ikut berlomba. Anda menyalip orang di posisi no 2. berapa posisi sekrang', '3', 'A', 'A', '2', 'B', '-', 'C', '-', 'D', '-'),
	(10, 'hari apa kemarin kalau lusa hari kamis\r\n', '3', 'A', 'A', 'senin', 'B', '-', 'C', '-', 'D', '-');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
